
import { AsyncStorage } from 'react-native'
// const STORAGE_NAME = '@LocalDB:AsyncStorDem0'

export default (STORAGE_NAME) => {
  
  const AsyncStorageHelper = {
    setData: async function (data) {
      try {
        await AsyncStorage.setItem(STORAGE_NAME, JSON.stringify(data))
        return data
      } catch (error) {
        console.log('setdata err', error)        
        return data
      }

    },
    getDataArray: async function () {
      try {
        const value = await AsyncStorage.getItem(STORAGE_NAME)
        if (value !== null) {
          // We have data!!
          return JSON.parse(value)
        }
      } catch (error) {
        return []
      }
      return []
    },
    getDataObject: async function () {
      try {
        const value = await AsyncStorage.getItem(STORAGE_NAME)
        if (value !== null) {
          // We have data!!
          return JSON.parse(value)
        }
      } catch (error) {
        return null
      }
      return null
    },
    addData: async function (record) {
      try {
        let currentData = await AsyncStorageHelper.getData() || []
        currentData.push(record)
  
        await AsyncStorage.setItem(STORAGE_NAME, JSON.stringify(currentData))
        return currentData
      } catch (error) {
        console.log('err', error)        
        return []
      }
    },
    clearData: async function(){
      await AsyncStorage.removeItem(STORAGE_NAME)
    },
    deleteData: async function(id){
      try {
        let currentData = await AsyncStorageHelper.getData() || []
        
        const newArr = currentData.filter((x) => x.id != id)
    
        await AsyncStorage.setItem(STORAGE_NAME, JSON.stringify(newArr))
        return newArr
      } catch (error) {
        console.log('err', error)        
        return []
      }
    },
    editData: async function(record){
      try {
        let currentData = await AsyncStorageHelper.getData() || []
        
        currentData.forEach((element, index) => {
          if(element.id === record.id) {
            currentData[index] = record
          }
        })
    
        await AsyncStorage.setItem(STORAGE_NAME, JSON.stringify(currentData))
        return currentData
      } catch (error) {
        console.log('err', error)        
        return []
      }
    },
  }
  return AsyncStorageHelper
}

// export default AsyncStorageHelper
  
