import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  geoTrackingRequest: ['data'],
  geoTrackingSuccess: ['payload'],
  geoTrackingFailure: null,
  // geoAction
  geoTrackingSetActive: ['isactive'],
  geoTrackingSetAction: ['act'],
  geoTrackingSetLastData: ['object'],

  geoTrackingStoreLocation: ['location'],
  geoTrackingGetStoredLocation: ['getall'],

  geoTrackingSendToServer: null,

  geoTrackingConfigure: null,

  geoTrackingSetActivity: ['activity'],
})

export const GeoTrackingTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null,
  //geo state
  isactive: false,
  action: 'nojob',
  last: {
    _id: 0,
    latitude: 0,
    longitude: 0,
    altitude: 0,
    time: 0,
    accuracy: 0,
    action: 'nojob'
  },
  activity: null
})

/* ------------- Selectors ------------- */

export const GeoTrackingSelectors = {
  getData: state => state.data
}

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }) =>
  state.merge({ fetching: true, data, payload: null })

// successful api lookup
export const success = (state, action) => {
  const { payload } = action
  return state.merge({ fetching: false, error: null, payload })
}

// Something went wrong somewhere.
export const failure = state =>
  state.merge({ fetching: false, error: true, payload: null })

export const active = (state, action) => {
  const { isactive } = action
  return state.merge({ isactive })
}

export const action = (state, action) => {
  const { act } = action
  return state.merge({ action: act })
}

export const activity = (state, action) => {
  const { activity } = action
  return state.merge({ activity })
}

export const lastdata = (state, action) => {
  const { object } = action
  return state.merge({ last: object })
}
/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GEO_TRACKING_REQUEST]: request,
  [Types.GEO_TRACKING_SUCCESS]: success,
  [Types.GEO_TRACKING_FAILURE]: failure,
  //geo reducer
  [Types.GEO_TRACKING_SET_ACTIVE]: active,
  [Types.GEO_TRACKING_SET_ACTION]: action,
  [Types.GEO_TRACKING_SET_ACTIVITY]: activity,
  [Types.GEO_TRACKING_SET_LAST_DATA]: lastdata,
})
