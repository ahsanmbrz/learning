import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  authCheckSession: null,
  authRemoveSession: null,
  // authSessionExist: ['payload'],
  authSessionEmpty: null,

  authRequest: ['data'],
  authRequestRegister: ['data'],
  authSuccess: ['payload'],
  authFailure: null,

  // authOtpRequest: ['dataOtp'],
  // authOtpSuccess: ['payloadOtp'],
  // authOtpFailure: null,

  // authRequestGetNewOtp: ['data'],
  // authSuccessGetNewOtp: ['payload'],

  authReset: null,
})

export const AuthTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  session: null,
  data: null,
  fetching: null,
  payload: null,
  error: null,

  // dataOtp: null,
  // fetchingOtp: null,
  // payloadOtp: null,
  // errorOtp: null,

  // dataReqOtp: null,
  // fetchingReqOtp: null,
  // payloadReqOtp: null,
  // errorReqOtp: null,
})

/* ------------- Selectors ------------- */

export const AuthSelectors = {
  getPayload: state => state.auth.session
}

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }) =>
  state.merge({ fetching: true, data })
// export const requestotp = (state, { dataOtp }) =>
//   state.merge({ fetchingOtp: true, dataOtp })

// successful api lookup
export const success = (state, action) => {
  const { payload } = action  
  return state.merge({ fetching: false, error: null, payload })
}
// export const successotp = (state, action) => {
//   const { payloadOtp } = action
//   return state.merge({ fetchingOtp: false, errorOtp: null, payloadOtp, session: payloadOtp })
// }

// Something went wrong somewhere.
export const failure = state =>
  state.merge({ fetching: false, error: true })
// export const failureotp = state =>
//   state.merge({ fetchingOtp: false, errorOtp: true })

export const reset = state =>
  state.merge(INITIAL_STATE)


/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.AUTH_REQUEST]: request,
  [Types.AUTH_REQUEST_REGISTER]: request,
  [Types.AUTH_SUCCESS]: success,
  [Types.AUTH_FAILURE]: failure,

  // [Types.AUTH_OTP_REQUEST]: requestotp,
  // [Types.AUTH_OTP_SUCCESS]: successotp,
  // [Types.AUTH_OTP_FAILURE]: failureotp,

  // [Types.AUTH_REQUEST_GET_NEW_OTP]: request,
  // [Types.AUTH_SUCCESS_GET_NEW_OTP]: success,

  [Types.AUTH_RESET]: reset,
})
