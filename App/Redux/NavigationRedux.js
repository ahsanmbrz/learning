import { NavigationActions, StackActions } from 'react-navigation'
import AppNavigation from '../Navigation/AppNavigation'

const GOHOME = (state) => {  
  const oldState = AppNavigation.router.getStateForAction( state)
  const resetAction = StackActions.reset({
    index: 0,
    actions: [
      NavigationActions.navigate({ routeName: 'LaunchScreen' }),
    ],
  })

  return AppNavigation.router.getStateForAction(resetAction, oldState)
}

const GOLOGIN = (state) => {  
  const oldState = AppNavigation.router.getStateForAction( state)
  const resetAction = StackActions.reset({
    index: 0,
    actions: [
      NavigationActions.navigate({ routeName: 'LoginScreen' }),
    ],
  })

  return AppNavigation.router.getStateForAction(resetAction, oldState)
}


export const reducer = (state, action) => {
  // console.log('action', action)
  switch (action.type) {
  case 'AUTH_SUCCESS':
    return GOHOME(state)
  case 'AUTH_FAILURE':
    return GOLOGIN(state)
  }

  const newState = AppNavigation.router.getStateForAction(action, state)
  return newState || state
}
