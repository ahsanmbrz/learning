export default {
  // HOST_URL : 'http://192.168.10.2:9981/'
  // HOST_URL : 'http://loginusadev.online:9981/'
  HOST_URL : 'http://localhost:9981/',

  CONTEXT: 'app',
  AUTH_SESSION_KEY: 'GeoTrack:AssEn@',
  STORAGE_LOCATION: '@geoTracking:Location',
  STORAGE_LASTSYNC: '@geoTracking:LastSync'
}
  