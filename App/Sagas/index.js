import { takeLatest, all } from 'redux-saga/effects'
import API from '../Services/Api'
import FixtureAPI from '../Services/FixtureApi'
import DebugConfig from '../Config/DebugConfig'

/* ------------- Types ------------- */

import { StartupTypes } from '../Redux/StartupRedux'
import { GithubTypes } from '../Redux/GithubRedux'
import { GeoTrackingTypes } from '../Redux/GeoTrackingRedux'
import { AuthTypes } from '../Redux/AuthRedux'

/* ------------- Sagas ------------- */

import { startup } from './StartupSagas'
import { getUserAvatar } from './GithubSagas'
import { storeLocation
  ,configureGeoTracking
  ,getStoredLocation
  ,sendLocationToServer
} from './GeoTrackingSagas'

import { 
  doLogin,
  checkSession,
  deleteSession,
  doRegister,  
} from './AuthSagas'
/* ------------- API ------------- */

// The API we use is only used from Sagas, so we create it here and pass along
// to the sagas which need it.
const api = DebugConfig.useFixtures ? FixtureAPI : API.create()

/* ------------- Connect Types To Sagas ------------- */

export default function * root () {
  yield all([
    // some sagas only receive an action
    takeLatest(StartupTypes.STARTUP, startup),

    // some sagas receive extra parameters in addition to an action
    takeLatest(GithubTypes.USER_REQUEST, getUserAvatar, api),

    takeLatest(GeoTrackingTypes.GEO_TRACKING_CONFIGURE, configureGeoTracking),
    takeLatest(GeoTrackingTypes.GEO_TRACKING_STORE_LOCATION, storeLocation),
    takeLatest(GeoTrackingTypes.GEO_TRACKING_GET_STORED_LOCATION, getStoredLocation),

    takeLatest(GeoTrackingTypes.GEO_TRACKING_SEND_TO_SERVER, sendLocationToServer, api),

    //Auth
    takeLatest(AuthTypes.AUTH_REQUEST, doLogin, api),
    takeLatest(AuthTypes.AUTH_REQUEST_REGISTER, doRegister, api),
    takeLatest(AuthTypes.AUTH_CHECK_SESSION, checkSession),
    takeLatest(AuthTypes.AUTH_REMOVE_SESSION, deleteSession),
  ])
}
