/* ***********************************************************
* A short word on how to use this automagically generated file.
* We're often asked in the ignite gitter channel how to connect
* to a to a third party api, so we thought we'd demonstrate - but
* you should know you can use sagas for other flow control too.
*
* Other points:
*  - You'll need to add this saga to sagas/index.js
*  - This template uses the api declared in sagas/index.js, so
*    you'll need to define a constant in that file.
*************************************************************/
import { AsyncStorage } from 'react-native'
import { call, put } from 'redux-saga/effects'
import AuthActions from '../Redux/AuthRedux'
import { Toast } from 'native-base'
import AppConfig from '../Config/StaticVar'


// import { AuthSelectors } from '../Redux/AuthRedux'


export async function setAuthSession(objectLogin) {
  try {
    
    await AsyncStorage.setItem(AppConfig.AUTH_SESSION_KEY, JSON.stringify(objectLogin))
  } catch (error) {
    console.error('error saving auth : ', error)
  }
}

export async function getAuthSession() {
  try {
    const value = await AsyncStorage.getItem(AppConfig.AUTH_SESSION_KEY)
    return JSON.parse(value)
  } catch (error) {
    console.error('getting session auth : ', error)
  }
}

export async function removeSession() {
  try {
    await AsyncStorage.removeItem(AppConfig.AUTH_SESSION_KEY)
  } catch (error) {
    console.error('getting session auth : ', error)
  }
}


export function * doLogin (api, action) {
  const { data } = action  
  const response = yield call(api.doLogin, data)
  console.log('response', response)
  
  // success?
  if (response.ok) {
    const responseData = response.data
    yield call(setAuthSession, responseData)
    yield put(AuthActions.authSuccess(responseData))     
  } else {    
    if (response.problem === 'NETWORK_ERROR') {
      Toast.show({
        text: 'Gagal Terhubung, Harap Periksa Koneksi Anda...',
        type: 'danger',
        duration: 1100
      })
    } 
    Toast.show({
      text: response.data,
      type: 'danger',
      duration: 1100
    })
    yield put(AuthActions.authFailure())
  }
}

export function * doRegister (api, action) {
  const { data } = action  
  const response = yield call(api.doRegister, data)
  console.log('reg response', response)
  
  // success?
  if (response.ok) {
    const responseData = response.data
    yield call(setAuthSession, responseData)
    Toast.show({
      text: 'Register Akun, Sukses !',
      type: 'success'
    })
    yield put(AuthActions.authSuccess(responseData))     
  } else {    
    if (response.problem === 'NETWORK_ERROR') {
      Toast.show({
        text: 'Gagal TerhuTouchableOpacityung, Harap Periksa Koneksi Anda...',
        type: 'danger',
        duration: 1100
      })
    } 
    Toast.show({
      text: response.data,
      type: 'danger',
      duration: 1100
    })
    yield put(AuthActions.authFailure())
  }
}


// export function * validateOTP (api, action) {
//   const { dataOtp } = action  
//   console.log('parameter validateotp', dataOtp)
  
//   const response = yield call(api.validateOTP, dataOtp)
//   // console.log('response', response)
  
//   // success?
//   if (response.ok) {
//     const responseData = response.data.data
//     console.log('cek responseData otp validate sukses', responseData)
//     //DISINI SET SESSION
//     yield call(setAuthSession, responseData)
//     yield put(AuthActions.authOtpSuccess(responseData)) 
//   } else {
//     console.log('response ERROR', response.problem)
//     if (response.problem === 'NETWORK_ERROR') {
//       Toast.show({
//         text: 'Gagal Terhubung, Harap Periksa Koneksi Anda...',
//         type: 'danger',
//         duration: 1100
//       })
//     } else {
//       Toast.show({
//         text: response.data.message,
//         type: 'danger',
//         duration: 1100
//       })
//     }
//     yield put(AuthActions.authOtpFailure())
//   }
// }

// export function * reqOtp (api, action) {
//   const { data } = action
//   // get current data from Store
//   // const currentData = yield select(AuthSelectors.getData)
//   // make the call to the api
  
  
//   const response = yield call(api.doLogin, data)
  
//   // success?
//   if (response.ok) {
//     const responseData = response.data.data
//     yield put(AuthActions.authSuccessGetNewOtp(responseData))     
//   } else {    
//     if (response.problem === 'NETWORK_ERROR') {
//       Toast.show({
//         text: 'Gagal Terhubung, Harap Periksa Koneksi Anda...',
//         type: 'danger',
//         duration: 1100
//       })
//     } else {
//       Toast.show({
//         text: response.data.message,
//         type: 'danger',
//         duration: 1100
//       })
//     }
//     yield put(AuthActions.authFailure())
//   }
// }

export function * checkSession () {
  console.log('OI')
  
  const sessionExists = yield call(getAuthSession)
  console.log('sessionExists', sessionExists)
  // success?
  if (sessionExists) {
    yield put(AuthActions.authSuccess(sessionExists))     
  } else {    
    yield put(AuthActions.authFailure())
  }
}


export function * deleteSession () {
    
  yield call(removeSession)
  console.log('terjalankan')
  Toast.show({
    text: 'Anda berhasil Logout',
    type: 'success',
    duration: 1100
  })
  yield put(AuthActions.authFailure())
  
}