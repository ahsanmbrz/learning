/* ***********************************************************
* A short word on how to use this automagically generated file.
* We're often asked in the ignite gitter channel how to connect
* to a to a third party api, so we thought we'd demonstrate - but
* you should know you can use sagas for other flow control too.
*
* Other points:
*  - You'll need to add this saga to sagas/index.js
*  - This template uses the api declared in sagas/index.js, so
*    you'll need to define a constant in that file.
*************************************************************/

import { call, put } from 'redux-saga/effects'
import { Alert } from 'react-native'
import _ from 'lodash'
import moment from 'moment'
import 'moment/locale/id'
import GeoTrackingActions from '../Redux/GeoTrackingRedux'
import AsyncStorageHelper from '../Lib/AsyncStorageHelper'
import StaticVar from '../Config/StaticVar'
// import { GeoTrackingSelectors } from '../Redux/GeoTrackingRedux'
// const STORAGE_LOCATION = '@geoTracking:Location'
// const STORAGE_LASTSYNC = '@geoTracking:LastSync'

// async function setDataToLocalStorage(data) {
//   try {
//     await AsyncStorage.setItem(STORAGE_LOCATION, JSON.stringify(data))
//     // return data
//   } catch (error) {
//     // return []
//   }
// }

// async function getDataFromLocalStorage() {
//   try {
//     const value = await AsyncStorage.getItem(STORAGE_LOCATION)
//     if (value !== null) {
//       // We have data!!
//       return JSON.parse(value)
//     }
//   } catch (error) {
//     return []
//   }
//   return []
// }

export function * storeLocation (action) {
  const { location } = action
  const session = yield call(AsyncStorageHelper(StaticVar.AUTH_SESSION_KEY).getDataArray)
  const datas = yield call(AsyncStorageHelper(`${StaticVar.STORAGE_LOCATION}@${session.username}`).getDataArray) //yield call(getDataFromLocalStorage)  
  //tambah property username disini  
  // console.log('location nyaaaaaa', location)  
  const modifiedLocation = { ...location, username : session.username}
  // console.log('mod location nyaaaaaa', modifiedLocation)  
  //end: tambah property username disini

  // console.log('data to store', [...datas, modifiedLocation])
  
  // yield call(setDataToLocalStorage, [...datas, modifiedLocation]) //DEPRECATED 
  yield call(AsyncStorageHelper(`${StaticVar.STORAGE_LOCATION}@${session.username}`).setData, [...datas, modifiedLocation])
  yield put(GeoTrackingActions.geoTrackingSendToServer())
}

export function * getStoredLocation (action) {
  const { getall } = action
  const validatedGetAll = getall || false
  const session = yield call(AsyncStorageHelper(StaticVar.AUTH_SESSION_KEY).getDataArray)

  const datas = yield call(AsyncStorageHelper(`${StaticVar.STORAGE_LOCATION}@${session.username}`).getDataArray)
  
  if (validatedGetAll) {
    console.log(datas) //cetak smua
  } else {    
    const filteredDatas = datas.filter(data => data.synced === false)
    console.log(filteredDatas) //filter hanya yg synced = false    
  }
  
}

export function * sendLocationToServer (api, /*action*/) {
  // const { getall } = action
  const session = yield call(AsyncStorageHelper(StaticVar.AUTH_SESSION_KEY).getDataArray)
  const datas = yield call(AsyncStorageHelper(`${StaticVar.STORAGE_LOCATION}@${session.username}`).getDataArray)
  const lastSync = yield call(AsyncStorageHelper(`${StaticVar.STORAGE_LASTSYNC}@${session.username}`).getDataObject) //yield call(getLastSync)
  
  console.log('data to send', datas)
  

  const orderedDatas = _.orderBy(datas, 'created', 'desc')
  // for (const tes of orderedDatas) {
  //   const date = moment(tes.created,moment.defaultFormat).format('DD-MM-YYYY HH:mm:ss')
  //   console.log(date)
  // }
  
  let filteredDatas = orderedDatas
  console.log('lastSync', moment(lastSync,moment.defaultFormat).format('HH:mm:ss'))

  if (lastSync) {
    filteredDatas = orderedDatas.filter(data => {
      const momentDate = moment(data.created,moment.defaultFormat)
      return momentDate.isAfter(moment(lastSync,moment.defaultFormat))
    })  
  }
  // console.log('filteredDatas', filteredDatas) 

  const response = yield call(api.sendLocation, { locations: filteredDatas })
  console.log('response', response)
  
  // // success?
  if (response.ok) {
    // You might need to change the response here - do this with a 'transform',
    // located in ../Transforms/. Otherwise, just pass the data back from the api.
    // yield put(GeoTrackingActions.geoTrackingSuccess(response.data))
    console.log('terhubung')    
    yield call(AsyncStorageHelper(`${StaticVar.STORAGE_LASTSYNC}@${session.username}`).setData, filteredDatas[0].created) // setLastSync(filteredDatas[0].created)
  } else {
    console.log('GAGAL terhubung')
    if (response.status === 500) {
      Alert.alert(response.data)
    }     
    // yield put(GeoTrackingActions.geoTrackingFailure())
  }    
}

// async function updateSynced(datas) {
//   console.log(_.keyBy(datas, '_id'))
//   // 
// }

// async function getLastSync() {
//   return await AsyncStorage.getItem(STORAGE_LASTSYNC)
// }

// async function setLastSync(data) {
//   AsyncStorage.setItem(STORAGE_LASTSYNC, data)
// }

export function * configureGeoTracking (api, action) {
  const { data } = action
  // get current data from Store
  // const currentData = yield select(GeoTrackingSelectors.getDataArray)
  // make the call to the api
  const response = yield call(api.getgeoTracking, data)

  // success?
  if (response.ok) {
    // You might need to change the response here - do this with a 'transform',
    // located in ../Transforms/. Otherwise, just pass the data back from the api.
    yield put(GeoTrackingActions.geoTrackingSuccess(response.data))
  } else {
    yield put(GeoTrackingActions.geoTrackingFailure())
  }
}
