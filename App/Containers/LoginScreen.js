import React, { Component } from 'react'
import { Animated, Keyboard, Platform } from 'react-native'
import {
  Container,
  Text,
  // View,
  Form,
  Item,
  // Label,
  Input,
  Button,
  Toast,
  Content,
  H1,
  View,
  Spinner,
  // Label,
  // CheckBox
} from 'native-base'
// import { StackActions, NavigationActions } from 'react-navigation'
import { connect } from 'react-redux'

// Add Actions - replace 'Your' with whatever your reducer is called :)
import AuthActions from '../Redux/AuthRedux'

// Styles
import styles from './Styles/LoginScreenStyle'
import Images from '../Themes/Images'
// import TelephoneInput from '../Components/TelephoneInput';

class LoginScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      username: '',
      password: '',
      isLogin: true,
      name: '',
      // isstaff: false
    }

    this.height = new Animated.Value(200)
    this.width = new Animated.Value(200)
    this.top = new Animated.Value(-20)
  }
  componentWillMount() {
    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      this._keyboardDidShow
    )
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      this._keyboardDidHide
    )
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove()
    this.keyboardDidHideListener.remove()
  }

  componentDidMount() {
    this.props.resetState()
    // this.setState({
    //   pickerData: this.phone.getPickerData(),
    // })
  }
  

  _keyboardDidShow = () => {
    Animated.parallel([
      Animated.timing(this.height, {
        toValue: 0,
        duration: 300
      }),
      Animated.timing(this.width, {
        toValue: 0,
        duration: 300
      }),
      Animated.timing(this.top, {
        toValue: Platform.OS === 'ios' ? 90 : 0,
        duration: 300
      })
    ]).start()
  };

  _keyboardDidHide = () => {
    Animated.parallel([
      Animated.timing(this.height, {
        toValue: 200,
        duration: 300
      }),
      Animated.timing(this.width, {
        toValue: 200,
        duration: 300
      }),
      Animated.timing(this.top, {
        toValue: -20,
        duration: 300
      })
    ]).start()
  };

  logreg() {
    let isNotValid = false
    if (this.state.isLogin){
      isNotValid = !(this.state.username && this.state.password)
    } else {
      //validasi register
      isNotValid = !(this.state.username && this.state.password && this.state.name)
    }

    if (isNotValid) {
      Toast.show({
        text: (!this.state.isLogin) ? 'Name / ' : '' + 'User / Pass Harus Terisi!',
        type: 'danger',
        duration: 1000
      })
    } else {      
      if (this.state.isLogin){
        this.props.doLogin({ username: this.state.username, password: this.state.password })
      } else {
        this.props.doRegister({ 
          username: this.state.username, 
          password: this.state.password,
          name: this.state.name, 
          // isstaff: this.state.isstaff,
        })
      }
      // console.log('zz', this.state.telepon)
    }
  }

  render () {
    return (
      <Container style={styles.container}>
        <Content
          bounces={false}
          contentContainerStyle={{
            flex: 1,
            justifyContent: 'space-around'
          }}
        >
          <View style={{flexDirection: 'column', alignItems: 'center' }}>
            <H1>Geo Tracking</H1>
          </View>
          {/* <Animated.Image
            source={Images.launch}
            style={[
              styles.logo,
              {
                height: this.height,
                width: this.width,
                top: this.top
              }
            ]}
          /> */}
          <View style={{ alignSelf: 'center', flexDirection: 'row'}}>
            {
              (this.state.isLogin) ?
                (
                  <Text>
                    {/* <TouchableOpacity onPress={()=> this.setState({ isLogin: false })}> */}
                    <Text style={{ fontSize: 11, color: 'blue' }} onPress={()=> this.setState({ isLogin: false })}>Register,</Text>
                    {/* </TouchableOpacity> */}
                    <Text style={{ fontSize: 11 }}> Bila belum memiliki akun</Text>
                  </Text>
                ) :
                <Text>
                  {/* <TouchableOpacity onPress={()=> this.setState({ isLogin: true })}> */}
                  <Text style={{ fontSize: 11, color: 'green' }} onPress={()=> this.setState({ isLogin: true })}>Login,</Text>
                  {/* </TouchableOpacity> */}
                  <Text style={{ fontSize: 11 }}> Bila sudah memiliki akun</Text>
                </Text>
            }
          </View>
          
          <Form style={styles.form}>
            {
              (this.state.isLogin) ? <Text style={{ alignSelf: 'center', color: 'green' }}>Login</Text> : 
                <Text style={{ alignSelf: 'center', color: 'blue' }}>Register</Text>
            }
            {
              (this.state.isLogin) ? null : 
                <Item underline>
                  <Input
                    onChangeText={name => this.setState({ name })}
                    placeholder="Name"
                    value={(this.state.name)}
                    returnKeyType={'next'}
                    autoFocus
                  />
                </Item>
            }
            <Item underline>
              <Input
                onChangeText={username => this.setState({ username })}
                placeholder="Username"
                value={(this.state.username)}
                returnKeyType={'next'}
                autoFocus
              />
            </Item>
            <Item underline>
              <Input
                secureTextEntry
                onChangeText={password => this.setState({ password })}
                value={this.state.password}
                placeholder="Password"
                returnKeyType={'go'}
                onSubmitEditing={() =>
                  this.onButtonPress(this)}
              />
            </Item>
            {/* {
              (this.state.isLogin) ? null : 
                <Item inlineLabel>
                  <Label>Is Staff</Label>
                  <CheckBox checked={this.state.isstaff} onPress={() => this.setState({ isstaff: !this.state.isstaff })}/>
                </Item>
            } */}
          </Form>
          {
            (this.props.auth.fetching === true) ? (
              <Spinner color={'blue'} />
            ) :
              (<Button full style={styles.button} onPress={() => this.logreg()}>
                {
                  (this.state.isLogin) ? <Text>Login</Text> : <Text>Register</Text> 
                }
              </Button>)
          }
        </Content>
      </Container>
    )
  }
}



const mapStateToProps = (state) => {
  return {
    auth: state.auth
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    doLogin: (param) => dispatch(AuthActions.authRequest(param)),
    doRegister: (param) => dispatch(AuthActions.authRequestRegister(param)),
    resetState: () => dispatch(AuthActions.authReset()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen)
