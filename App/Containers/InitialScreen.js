import React, { Component } from 'react'
import { Animated } from 'react-native'
import {
  Container,
  Content,
  Spinner,
  Text
} from 'native-base'
// import { StackActions, NavigationActions } from 'react-navigation'
import { connect } from 'react-redux'

// Add Actions - replace 'Your' with whatever your reducer is called :)
import AuthActions from '../Redux/AuthRedux'

// Styles
// import Images from '../Themes/Images'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
// import styles from './Styles/InitialScreenStyle'

class InitialScreen extends Component {
  constructor(props) {
    super(props)

    this.height = new Animated.Value(200)
    this.width = new Animated.Value(200)
    this.top = new Animated.Value(-17)
  }

  componentDidMount() {
    this.props.checkSession()
  }
  render () {
    return (
      <Container>
        <Content
          bounces={false}
          contentContainerStyle={{
            flex: 1,
            justifyContent: 'center'
          }}
        >
          <Text style={{ alignSelf: 'center' }}>Checking Sesion ...</Text>
          <Spinner color="blue" />
        </Content>
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.auth
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    checkSession: () => dispatch(AuthActions.authCheckSession()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(InitialScreen)
