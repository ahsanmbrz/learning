import React, { Component } from 'react'
import { Container, Header, Title, Content, Button, Left, Right, Body, Icon, Text, Switch, View, Grid, Row, Col } from 'native-base'
import { Alert, AsyncStorage, PermissionsAndroid, ToastAndroid } from 'react-native'
import BackgroundGeolocation from '@mauron85/react-native-background-geolocation'
import ActivityRecognition from 'react-native-activity-recognition'
import { connect } from 'react-redux'

import uuid from 'uuid/v1'
import moment from 'moment'
import 'moment/locale/id'

import GeoTrackingActions from '../Redux/GeoTrackingRedux'
import AuthActions from '../Redux/AuthRedux'
// import { Images } from '../Themes'

// Styles
// import styles from './Styles/LaunchScreenStyles'
// const SERVER_BASE_URL = 'http://192.168.10.2:9981/'

class LaunchScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      granted: false
    }
    // this.unsubscribe = this.unsubscribe.bind(this)
  }

  componentDidMount() {
    this.permissionPromptAndBackgroundConfig()
    
  }

  async permissionPromptAndBackgroundConfig() {
    try {
      const result = await PermissionsAndroid.requestMultiple(
        [
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
          // PermissionsAndroid.PERMISSIONS.BODY_SENSORS,
          PermissionsAndroid.PERMISSIONS.READ_PHONE_STATE,
        ]
      )
      console.log('result', result)
      if (result['android.permission.READ_PHONE_STATE'] === PermissionsAndroid.RESULTS.GRANTED &&
        // result.android.permission.BODY_SENSORS === PermissionsAndroid.RESULTS.GRANTED &&
        result['android.permission.ACCESS_COARSE_LOCATION'] === PermissionsAndroid.RESULTS.GRANTED &&
        result['android.permission.ACCESS_FINE_LOCATION'] === PermissionsAndroid.RESULTS.GRANTED) {
        this.setState({
          granted: true
        })
        console.log('permission granted')
        this.configureGeoTracking()
        // this.startActivityRecognition()
      } else {
        this.setState({
          granted: false
        })
      }
    } catch (err) {
      console.warn(err)
    }
  }

  componentWillUnmount() {
    // unregister all event listeners    
    // if (eventSubscription !== null) {
    //   eventSubscription.remove()
    //   eventSubscription = null
    // }
    BackgroundGeolocation.removeAllListeners()

  }

  render () {
    if (!this.state.granted) {
      return null
    }
    return (
      <Container>
        <Header noLeft>
          <Left>
            <Button transparent>
              <Icon name='menu' />
            </Button>
          </Left>
          <Body>
            <Title>Geo Tracking</Title>
          </Body>
          <Right>
            <View style={{ flexDirection: 'row', alignSelf: 'center' }}>
              <Text style={{ color: 'white', fontSize: 13, marginRight: 5 }} >Enable Tracking</Text>
              <Switch 
                value={this.props.geotracking.isactive} 
                onValueChange={() => {
                  this.switchHandler()                
                }}
                trackColor={{}}
              />
            </View>
            <Button transparent onPress={() => {
              this.props.storeLocation({nama : 'f', asal: 't'})
            }}>
              <Icon name='bug' />
            </Button>
            {/* <Button transparent onPress={() => {
              this.props.getStoredLocation()
            }}>
              <Icon name='bug' />
            </Button>
            <Button transparent onPress={() => {
              AsyncStorage.clear()
            }}>
              <Icon name='refresh' />
            </Button> */}
            <Button transparent onPress={() => {
              this.props.logOut()
            }}>
              <Icon name='log-out' />
            </Button>
          </Right>
        </Header>
        <Content contentContainerStyle={{ flex: 1 }}>
          <View style={{ flex: 0.85, justifyContent: 'space-evenly', marginHorizontal: 15 }}>
            <Button full dark={this.props.geotracking.action !== 'prejobmeeting'} danger={this.props.geotracking.action === 'prejobmeeting'} onPress={() => this.props.setAction('prejobmeeting')}>
              <Text uppercase={false}>Pre Job Meeting</Text>
            </Button>
            <Button full dark={this.props.geotracking.action !== 'preops'} danger={this.props.geotracking.action === 'preops'} onPress={() => this.props.setAction('preops')}>
              <Text uppercase={false}>Pre Ops</Text>
            </Button>
            <Button full dark={this.props.geotracking.action !== 'ops'} danger={this.props.geotracking.action === 'ops'} onPress={() => this.props.setAction('ops')}>
              <Text uppercase={false}>Ops</Text>
            </Button>

            <Button full dark={this.props.geotracking.action !== 'nojob'} danger={this.props.geotracking.action === 'nojob'} onPress={() => this.props.setAction('nojob')}>
              <Text uppercase={false}>No Job</Text>
            </Button>
          </View>
          <View style={{ flex: 0.15, }}>
            <Grid>
              <Row>
                <Text style={{ fontSize: 13 }}>{`ID : ${this.props.geotracking.last._id}`}</Text>
              </Row>
              <Row>
                <Col size={1}>
                  <Text style={{ fontSize: 13 }}>Lat</Text>
                </Col>
                <Col size={3}>
                  <Text style={{ fontSize: 13 }}>{this.props.geotracking.last.latitude}</Text>
                </Col>

                <Col size={1}>
                  <Text style={{ fontSize: 13 }}>time</Text>
                </Col>
                <Col size={3}>
                  <Text style={{ fontSize: 13 }}>{this.props.geotracking.last.time}</Text>
                </Col>
              </Row>
              <Row>
                <Col size={1}>
                  <Text style={{ fontSize: 13 }}>Long</Text>
                </Col>
                <Col size={3}>
                  <Text style={{ fontSize: 13 }}>{this.props.geotracking.last.longitude}</Text>
                </Col>
                <Col size={1}>
                  <Text style={{ fontSize: 13 }}>action</Text>
                </Col>
                <Col size={3}>
                  <Text style={{ fontSize: 13 }}>{this.props.geotracking.last.action}</Text>
                </Col>
              </Row>
              <Row>
                <Text style={{ fontSize: 13 }}>{`Activity : ${(this.props.geotracking.activity) ? `[${this.props.geotracking.activity.confidence}] ${this.props.geotracking.activity.type}` : '-'}`}</Text>
              </Row>
            </Grid>
          </View>
        </Content>
      </Container>
    )
  }

  switchHandler() {
    BackgroundGeolocation.checkStatus(status => {
      
      console.log('[INFO] BackgroundGeolocation service is running', status.isRunning)
      console.log('[INFO] BackgroundGeolocation services enabled', status.locationServicesEnabled)
      console.log('[INFO] BackgroundGeolocation auth status: ' + status.authorization)
      this.props.setActive(status.isRunning)
      // you don't need to check status before start (this is just the example)
      if (!status.isRunning) {
        // this.configureGeoTracking()
        BackgroundGeolocation.start()  //triggers start
        //activity
        // this.startActivityRecognition()
      } else {
        BackgroundGeolocation.stop()   //triggers stop
        // BackgroundGeolocation.removeAllListeners()
        // this.stopActivityRecognition()
      }
    })
  }

  startActivityRecognition() {
    this.unsubscribe = ActivityRecognition.subscribe(detectedActivities => {
      const mostProbableActivity = detectedActivities.sorted[0]
    
      console.log('activity', mostProbableActivity)
      console.log('act event', mostProbableActivity)    
      ToastAndroid.show(JSON.stringify(mostProbableActivity), ToastAndroid.SHORT)
      // Toast.show({
      //   text: JSON.stringify(mostProbableActivity)
      // })      
      this.props.setActivity(mostProbableActivity)
    })

    const detectionIntervalMillis = 4500
    ActivityRecognition.start(detectionIntervalMillis)
  }

  stopActivityRecognition() {
    ActivityRecognition.stop()
    this.unsubscribe()
  }

  configureGeoTracking() {
    BackgroundGeolocation.configure({
      desiredAccuracy: BackgroundGeolocation.HIGH_ACCURACY,
      stationaryRadius: 7, //10 - 50
      distanceFilter: 7,                                     //PENTING
      notificationTitle: 'Background tracking',
      notificationText: 'enabled',
      debug: true,                                                //PENTING true
      startOnBoot: false,
      stopOnTerminate: false, //true
      locationProvider: BackgroundGeolocation.ACTIVITY_PROVIDER, //(this.props.geotracking.action === 'ops') ? BackgroundGeolocation.ACTIVITY_PROVIDER : BackgroundGeolocation.DISTANCE_FILTER_PROVIDER, //DISTANCE_FILTER_PROVIDER
      interval: 7000,                                            //PENTING 10000
      fastestInterval: 5000,                                      //PENTING //5000
      activitiesInterval: 7000,                                  //PENTING //10000
      stopOnStillActivity: false,                                 
      // url: SERVER_BASE_URL+'locations',                                                  //PENTING // NULL DULU, contoh nilai : 'http://192.168.81.15:3000/location'
      // syncUrl: SERVER_BASE_URL+'sync',                                              //PENTING
      // syncThreshold: 100,
      httpHeaders: {                                              //PENTING
        'X-FOO': 'bar'
      },
      // customize post properties
      postTemplate: {                                             //PENTING
        lat: '@latitude',
        lon: '@longitude',
        foo: 'bar' // you can also add your own properties
      }
    })

    BackgroundGeolocation.on('location', (location) => {
      // handle your locations here
      // to perform long running operation on iOS
      // you need to create background task
      if (location.accuracy < 50) {
        moment.locale('id')
        const currentDate = moment().toDate()  
        const last = {
        // id: location.id,
          _id: uuid(),
          latitude: location.latitude,
          longitude: location.longitude,
          altitude: location.altitude,
          time: location.time,
          accuracy: location.accuracy,
          action: this.props.geotracking.action || 'nojob',
          // synced: false,
          created: currentDate,
          ...location,
          activity: this.props.geotracking.activity
        }
        this.props.setLastData(last)

        // console.log('lokasinya [extd]', last)
      
        this.props.storeLocation(last)

        // BackgroundGeolocation.startTask(taskKey => {
        // // execute long running task
        // // eg. ajax post location
        // // IMPORTANT: task has to be ended by endTask
        //   BackgroundGeolocation.endTask(taskKey)
        // })
      }
    })

    BackgroundGeolocation.on('stationary', (stationaryLocation) => {
      // handle stationary locations here
      // Actions.sendLocation(stationaryLocation);
      // Toast.show({
      //   text: JSON.stringify(stationaryLocation)
      // })
      if (stationaryLocation.accuracy < 60) {
        moment.locale('id')
        const currentDate = moment().toDate()  
        const last = {
        // id: location.id,
          _id: uuid(),
          latitude: stationaryLocation.latitude,
          longitude: stationaryLocation.longitude,
          altitude: stationaryLocation.altitude,
          time: stationaryLocation.time,
          accuracy: stationaryLocation.accuracy,
          action: this.props.geotracking.action || 'nojob',
          // synced: false,
          created: currentDate,
          ...stationaryLocation,
          activity: this.props.geotracking.activity
        }      

        this.props.storeLocation(last)
      }
      console.log('stationaryLocation', stationaryLocation)
    })

    BackgroundGeolocation.on('error', (error) => {
      console.log('[ERROR] BackgroundGeolocation error:', error)
    })

    BackgroundGeolocation.on('start', () => {
      console.log('[INFO] BackgroundGeolocation service has been started')
      this.props.setActive(true)      
      this.startActivityRecognition()

    })

    BackgroundGeolocation.on('stop', () => {
      console.log('[INFO] BackgroundGeolocation service has been stopped')
      this.props.setActive(false)
      this.stopActivityRecognition()
    })

    BackgroundGeolocation.on('authorization', (status) => {
      console.log('[INFO] BackgroundGeolocation authorization status: ' + status)
      if (status !== BackgroundGeolocation.AUTHORIZED) {
        // we need to set delay or otherwise alert may not be shown
        setTimeout(() =>
          Alert.alert('App requires location tracking permission', 'Would you like to open app settings?', [
            { text: 'Yes', onPress: () => BackgroundGeolocation.showAppSettings() },
            { text: 'No', onPress: () => console.log('No Pressed'), style: 'cancel' }
          ]), 1000)
      }
    })

    BackgroundGeolocation.on('background', () => {
      console.log('[INFO] App is in background')
    })

    BackgroundGeolocation.on('foreground', () => {
      console.log('[INFO] App is in foreground')
    })

    // BackgroundGeolocation.on('abort_requested', () => {
    //   console.log('[INFO] Server responded with 285 Updates Not Required')

    //   // Here we can decide whether we want stop the updates or not.
    //   // If you've configured the server to return 285, then it means the server does not require further update.
    //   // So the normal thing to do here would be to `BackgroundGeolocation.stop()`.
    //   // But you might be counting on it to receive location updates in the UI, so you could just reconfigure and set `url` to null.
    // })

    BackgroundGeolocation.on('http_authorization', () => {
      console.log('[INFO] App needs to authorize the http requests')
    })

    // BackgroundGeolocation.on('activity', activity => {
    //   console.log('activity', activity)
    //   console.log('act event', activity)    
    //   Toast.show({
    //     text: JSON.stringify(activity)
    //   })
    //   this.props.setActivity(activity)
    // })

    BackgroundGeolocation.checkStatus(status => {
      
      console.log('[INFO] BackgroundGeolocation service is running', status.isRunning)
      console.log('[INFO] BackgroundGeolocation services enabled', status.locationServicesEnabled)
      console.log('[INFO] BackgroundGeolocation auth status: ' + status.authorization)

      this.props.setActive(status.isRunning)
    })
    // you can also just start without checking for status
    // BackgroundGeolocation.start();
    // BackgroundGeolocation.checkStatus(status => {
    //   this.props.setActive(status.isRunning)
    // })
  }

}
const mapStateToProps = (state) => {
  return {
    geotracking: state.geotracking,
    auth: state.auth,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setActive: (isactive) => dispatch(GeoTrackingActions.geoTrackingSetActive(isactive)),
    setAction: (act) => dispatch(GeoTrackingActions.geoTrackingSetAction(act)),
    setActivity: (activity) => dispatch(GeoTrackingActions.geoTrackingSetActivity(activity)),
    setLastData: (object) => dispatch(GeoTrackingActions.geoTrackingSetLastData(object)),

    storeLocation: (location) => dispatch(GeoTrackingActions.geoTrackingStoreLocation(location)),
    getStoredLocation: (getall) => dispatch(GeoTrackingActions.geoTrackingGetStoredLocation(getall)),
    sendLocationToServer: () => dispatch(GeoTrackingActions.geoTrackingSendToServer()),
    logOut: () => dispatch(AuthActions.authRemoveSession()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LaunchScreen)